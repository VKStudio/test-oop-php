<?php

trait Hello {
    public function sayHello() {
        echo 'Hello World!';
    }
}

trait SpeakTrait {
    public function speak($message) {
        echo "{$this->name} says: $message\n";
    }
}