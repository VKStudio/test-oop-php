<?php

use Interfaces\UserInterface;
use Singleton\Singleton;
use Users\User;

require 'classes/User.php';
require 'classes/Animal.php';
require 'classes/Exceptions.php';
require 'classes/Singleton.php';


$dog1 = new Animal\Dog('Rex', 2);
$dog2 = new Animal\Dog('Pups', 5);


$cat1 = new Animal\Cat('Murzik', 10);

$person = new \Users\User('Viktor');

print_r(\Users\User::$gender);
print_r($person->getGender());

$cat1->newFunction('name', 1, 3);

function getUserData(UserInterface $user, string $phone) {
    $user->setPhoneNumber($phone);
    $user->getPhoneNumber();
}
getUserData($person, '0661471117');
//getUserData(new \Users\User('Viktor Kono'), '0661471117');


function getAllData(\Animal\Animal $obj) {
    $obj->move();

    if (!$obj->sayAge()) {
        throw new \Exception('the object doesnt have this method');
    }
}

try {
    getAllData($dog1);
    getAllData($cat1);
} catch (Exception $e) {
    echo '<br> exception: ' . $e->getMessage();
}

//$person->speak('Hello world!');
//$dog1->speak('#&$% #$#^');


function divide($dividend, $divisor) {
    if ($divisor == 0) {
        throw new CustomException('Деление на ноль недопустимо.');
    }
    return $dividend / $divisor;
}

try {
    $result = divide(10, 2);
    echo 'Результат деления: ' . $result;
} catch (CustomException $e) {
    echo $e->errorMessage();
} finally {
    echo "\nВыполнение блока finally\n";
}


$s1 = Singleton::getInstance();
$s2 = Singleton::getInstance();
if ($s1 === $s2) {
    echo "Singleton works, both variables contain the same instance.";
} else {
    echo "Singleton failed, variables contain different instances.";
}