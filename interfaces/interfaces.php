<?php
namespace Interfaces;

interface UserInterface {
    public function getPhoneNumber();
    public function setPhoneNumber($phone);
    public function displayUserData();
}