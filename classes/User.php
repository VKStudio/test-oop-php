<?php
namespace Users;

use Interfaces\UserInterface;
use SpeakTrait;

require './interfaces/interfaces.php';
require './traits/traits.php';

class User implements UserInterface {
    public string $name;
    protected int $age;
    protected string $phone;

    public static array $gender = ['male', 'female'];

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    use SpeakTrait;

    // Magic method __set / __get / __isset and others...
    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
    }

    public static function getGender(): array
    {
        return self::$gender;
    }

    /**
     * @param int $age
     */
    public function setAge(int $age)
    {
        $this->age = $age;
    }

    /**
     * @return int
     */
    public function getAge(): int
    {
        return $this->age;
    }

    /**
     * @param string
     */
    public function getPhoneNumber(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhoneNumber($phone)
    {
        $this->phone = $phone;
    }

    public function displayUserData()
    {
        echo 'User name: ' . $this->name . ' | Phone: ' . $this->phone;
    }
}