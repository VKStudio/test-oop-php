<?php

namespace Animal;

require_once 'traits/traits.php';

use SpeakTrait;

abstract class Animal
{
    use SpeakTrait;

    protected $name;
    protected $age;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    public function __set($name, $value) {
        $this->name = $value;
    }

    public function __get($name) {
        return $this->name;
    }

    //    public function __isset();
    //    public function __unset();

    public function __call($name, $arguments) {
        echo "Вызов метода '$name' "
            . implode(', ', $arguments). "\n";
    }

    public function getAge()
    {
        return $this->age;
    }

    abstract public function move();
}

class Dog extends Animal
{
    const PARENT = 'Wolf';

    public function move()
    {
        echo $this->name . ', lets move!';
    }

    public function sayAge()
    {
        echo 'Dog is ' . self::PARENT . ' who is ' . parent::getAge() . ' years old!';
    }
}

// Single tone
// Что может быть а что нет и как использовать.. отличие от класса
// статические методы


class Cat extends Animal
{
    public function move()
    {
        echo 'Cat ' . $this->name . ', lets move!';
    }
}