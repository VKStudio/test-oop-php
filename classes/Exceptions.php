<?php

class CustomException extends Exception {
    public function errorMessage() {
        return 'Ошибка: ' . $this->getMessage();
    }
}